package com.retoglobant.omar.service;

import com.retoglobant.omar.http.request.StudentRequest;
import com.retoglobant.omar.http.request.response.JsonResponse;
import com.retoglobant.omar.model.Student;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.util.List;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 10/09/2021
 * Time: 1:18 p. m.
 * Change Text
 */
public interface StudentService {

  Single<JsonResponse> createStudent(StudentRequest student);

  Completable updateStudent(StudentRequest student);

  Single<JsonResponse> getStudent(String id);

  Observable<Student> getStudentActives() throws InterruptedException;

  List<Student> getStudentActivesNoReact() throws InterruptedException;

  Completable deleteStudent(String id);

}
