package com.retoglobant.omar.service;

import com.retoglobant.omar.http.request.StudentRequest;
import com.retoglobant.omar.http.request.response.JsonResponse;
import com.retoglobant.omar.model.Student;
import com.retoglobant.omar.repository.StudentRepository;
import com.retoglobant.omar.util.Constantes;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 10/09/2021
 * Time: 12:46 p. m.
 * Change Text
 */
@Service
@Slf4j
public class StudentServiceImpl implements StudentService {

  @Autowired
  private StudentRepository repo;

  @Override
  public Single<JsonResponse> createStudent(StudentRequest request) {
    return Single.create(subscribe ->
      subscribe.onSuccess(JsonResponse.builder()
              .code(Constantes.CODE_RPTA_OK)
              .message(Constantes.MSGE_RPTA_OK)
              .result(repo.save(buildStudent(request)))
              .build())

    );
  }

  @Override
  public Completable updateStudent(StudentRequest request) {
    return Completable.create(subs -> {
      Optional<Student> studentBD = repo.findById(request.getId());
      if (studentBD.isEmpty()) {
        subs.onError(new EntityNotFoundException());
      } else {
        var student = studentBD.get();
        student.setName(request.getName());
        student.setCode(request.getCode());
        student.setGender(request.getGender());
        repo.save(student);
        // importante para que sea llamada por sus observadores
        subs.onComplete();
      }
    });
  }

  @Override
  public Single<JsonResponse> getStudent(String id) {
    return Single.create(subs ->
      subs.onSuccess(JsonResponse.builder()
              .code(Constantes.CODE_RPTA_OK)
              .message(Constantes.MSGE_RPTA_OK)
              .result(repo.findById(id)).build())
    );
  }

  @Override
  public Observable<Student> getStudentActives() throws InterruptedException {
    Random random = new Random();

    return Observable.create(x -> {
      repo.findAllByStatus(1).forEach(rep -> {
        try {
          Integer segundos = random.nextInt(3000 + 1000) + 1000;
          log.info("student: " + rep.getId() + " segundos: " + segundos);
          Thread.sleep(segundos);
          x.onNext(rep);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

      });
      x.onComplete();
//      return x;
    });
//            Thread.sleep(1000);

//      subs.onSuccess(JsonResponse.builder()
//              .code(Constantes.CODE_RPTA_OK)
//              .message(Constantes.MSGE_RPTA_OK)
//              .result().build())
  }

  @Override
  public List<Student> getStudentActivesNoReact() throws InterruptedException {
    Random random = new Random();
    return repo.findAllByStatus(1);
  }

  @Override
  public Completable deleteStudent(String id) {
    return Completable.create(subs -> {
      Optional<Student> studentBD = repo.findById(id);
      if (studentBD.isEmpty()) {
        subs.onError(new EntityNotFoundException());
      } else {
        var student = Student.builder().id(id).build();
        repo.delete(student);
        // importante para que sea llamada por sus observadores
        subs.onComplete();
      }
    });
  }

  private Student buildStudent(StudentRequest request) {
    var response = new Student();
    BeanUtils.copyProperties(request, response);
    response.setId(UUID.randomUUID().toString());
    response.setStatus(Constantes.STATUS_ACTIVE);
    return response;
  }
}
