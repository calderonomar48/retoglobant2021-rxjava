package com.retoglobant.omar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoRxJavaApplication {

  public static void main(String[] args) {
    SpringApplication.run(ProyectoRxJavaApplication.class, args);
  }

}
