package com.retoglobant.omar.controller;

import com.retoglobant.omar.http.request.StudentRequest;
import com.retoglobant.omar.http.request.response.JsonResponse;
import com.retoglobant.omar.model.Student;
import com.retoglobant.omar.service.StudentService;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 10/09/2021
 * Time: 1:16 p. m.
 * Change Text
 */
@RestController
@Slf4j
@RequestMapping(value = "/api/student")
public class StudentController {

  @Autowired
  private StudentService studentService;

  @PostMapping(
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE
  )
  public Single<ResponseEntity<JsonResponse>> createStudent(@RequestBody StudentRequest student) {
    return studentService.createStudent(student)
            .subscribeOn(Schedulers.io())
            .map(
                    stu -> new ResponseEntity<>(stu, HttpStatus.OK)
            ).doOnError(Throwable::printStackTrace);
  }

  @PutMapping(value = "/{id}",
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE
  )
  public Completable updateStudent(@PathVariable(value = "id") String id,
                                   @RequestBody StudentRequest request) {
    request.setId(id);
    return studentService.updateStudent(request)
            .subscribeOn(Schedulers.io());
  }

  @GetMapping(value = "/{id}",
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE
  )
  public Single<ResponseEntity<JsonResponse>> getStudent(@PathVariable String id) {
    return studentService.getStudent(id)
            .subscribeOn(Schedulers.io())
            .map(
                    stu -> new ResponseEntity<>(stu, HttpStatus.OK)
            ).doOnError(Throwable::printStackTrace);
  }

  @GetMapping(value = "/activesNoReact",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  public List<Student> getStudentActivesNoReactive() throws InterruptedException {
    Random random = new Random();
    return studentService.getStudentActivesNoReact().stream().map(rep -> {
      try{
//        Integer segundos = random.nextInt(5000 + 2000) + 1000;
                Integer segundos = random.nextInt(1000) + 1000;
        log.info("student: " + rep.getId() + " segundos: " + segundos);
        Thread.sleep(segundos);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return rep;
    }) .collect(Collectors.toList());
  }

  @GetMapping(value = "/actives",
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE
  )
  public Observable<Student> getStudentActives() throws InterruptedException {
    return studentService.getStudentActives()
            .subscribeOn(Schedulers.io())
            .map(x->x);
  }

  @DeleteMapping(value = "/{id}",
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE
  )
  public Completable deleteStudent(@PathVariable String id) {
    return studentService.deleteStudent(id)
            .subscribeOn(Schedulers.io());
  }

}
