package com.retoglobant.omar.repository;

import com.retoglobant.omar.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 10/09/2021
 * Time: 12:31 p. m.
 * Change Text
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, String> {

  List<Student> findAllByStatus(Integer status);

}
