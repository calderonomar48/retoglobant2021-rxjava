package com.retoglobant.omar.http.request;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 11/09/2021
 * Time: 8:17 p. m.
 * Change Text
 */
@Getter
@Setter
public class StudentRequest {

  private String id;

  private String name;

  private String code;

  private String gender;

}
