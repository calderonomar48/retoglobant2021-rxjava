package com.retoglobant.omar.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 10/09/2021
 * Time: 12:23 p. m.
 * Change Text
 */
@Entity
@Table(name = "students")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "name")
  private String name;

  @Column(name = "code")
  private String code;

  @Column(name = "gender")
  private String gender;

  @Column(name = "status")
  private Integer status;

}
