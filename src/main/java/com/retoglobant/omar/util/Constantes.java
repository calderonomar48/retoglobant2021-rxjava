package com.retoglobant.omar.util;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 12/09/2021
 * Time: 11:54 a. m.
 * Change Text
 */
public class Constantes {

  public  static final Integer STATUS_ACTIVE = 1;

  public  static final String CODE_RPTA_OK = "001";

  public  static final String MSGE_RPTA_OK = "consula ejecutada ol";

  private Constantes() {
  }

}
